from tkinter import *
from pynput.keyboard import Key,Controller
import time

root = Tk()
root.geometry("290x350")

keyboard = Controller()

e = Entry(root, width=35)
e.grid(row=9, column=0, padx=2, pady=2)
e.insert(0, "Enter message here to type")

def console_print(text):
    count = int(count_scale.get())
    for count in range(count):
        print(text)

def start():
    count = int(count_scale.get())
    time.sleep(delay.get())
    for count in range(count):
        keyboard.type(e.get())
        keyboard.press(Key.enter)
        keyboard.release(Key.enter)
        time.sleep(per_msg_delay.get())
    

frame = LabelFrame(root, text="Controls", padx=5, pady=5)
frame.grid(row=1, column=0)

start_button = Button(frame, text="Start", command=start)
start_button.grid(row=1, column=3)
b = Button(frame, text="Send to console", command=lambda: console_print(e.get()))
b.grid(row=2, column=3)

count_text = Label(root, text="Rate")
count_text.grid(row=3, column=0, padx=5, pady=5)

count_scale = Scale(root, from_=0, to =20, orient=HORIZONTAL)
count_scale.grid(row=4, column=0, padx=5, pady=5)

delay_text = Label(root, text="Initial Delay")
delay_text.grid(row=5, column=0, padx=5, pady=5)

delay = Scale(root, from_=0, to =20, orient=HORIZONTAL)
delay.grid(row=6, column=0, padx=5, pady=5)

per_msg_text = Label(root, text="Per Message Delay")
per_msg_text.grid(row=7, column=0, padx=5, pady=5)

per_msg_delay = Scale(root, from_=0, to =20, orient=HORIZONTAL)
per_msg_delay.grid(row=8, column=0, padx=5, pady=5)

root.mainloop()